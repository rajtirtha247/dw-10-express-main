import { Router } from "express";

let firstRouter = Router()

//url=localhost:8000, post at response "home post"
//url=localhost:8000, get at response "home get"

firstRouter
.route("/") // here "/" gives localhost:8000
.post((req,res) => {
    // console.log("home post")
    // console.log(req.body)
    console.log(req.query)
    res.json("home post")
})


firstRouter
.route("/name")

.post((req,res) => {
    console.log(req.query)
    res.json("name post")
})

firstRouter
.route("/product/:name") //: meaning yesko thau ma j bhayeni huncha--dynamic route parameter
.post((req,res)=>{
    res.json("i am product")
})


export default firstRouter

//so making api--defining task for each request is called making api
//shortcut in postman- ctrl+d, ctrl+e

//send data from postman


//get data