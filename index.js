//make an express application
//attach port to that application
import express, { json } from "express"
import firstRouter from "./SRC/router/firstRouter.js"
import secondRouter from "./SRC/router/secondRouter.js"
import thirdRouter from "./SRC/router/thirdRouter.mjs"

//making an express application
let expressApp = express()

expressApp.use(json()) //always place expressApp.use(json()) at the top

//attaching port
expressApp.listen(8000, ()=>{
    console.log("express application is listening at port 8000")
})

expressApp.use(firstRouter)

expressApp.use(secondRouter)

expressApp.use(thirdRouter)

//url= localhost:8000/product/1?name=tirtha&age=22&isMarried=false
//url=route?query
//route=localhost:8000
//route=baseURL/routeParameter1/routeParameter2
 //     localhost=8000/product/   b


